﻿using System;
//using Projet;
//using Mission;
//using Intervenant;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Métiers
{
    class Mission
    {
        #region CHAMPS
        private string _chaine;
        private string description;
        private int nbHeuresPrevues;
        private Dictionary<DateTime, int> _releveHoraire;
        #endregion

        #region PROPRIETES
        public string Chaine { get => _chaine; set => _chaine = value; }
        public string Description { get => description; set => description = value; }
        public int NbHeuresPrevues { get => nbHeuresPrevues; set => nbHeuresPrevues = value; }
        public Dictionary<DateTime, int> ReleveHoraire { get => _releveHoraire; set => _releveHoraire = value; }
        #endregion

        #region CONCEPTEURS

        #endregion

        #region METHODES

        #endregion

    }
}
