﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Métiers
{
    class Intervenant
    {
        #region CHAMPS
        private string _nom;
        private int _tauxHoraire;
        #endregion

        #region PROPRIETES
        public string Nom1 { get => _nom; set => _nom = value; }
        public int TauxHoraire { get => _tauxHoraire; set => _tauxHoraire = value; }       
        #endregion

        #region CONCEPTEURS

        #endregion

        #region METHODES

        #endregion
    }
}
